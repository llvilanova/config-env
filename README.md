My configuration for the shell and other programs.

Install
-------

Clone and run setup.sh to point all files and configuration directories to your
clone of the repository (can be run as many times as you want, in case you move
your clone around):

    git clone https://gitlab.com/llvilanova/config-env.git ~/config-env
    ~/config-env/setup.sh

This generates a small bash script that you should include from your main
bash configuration file (setup.sh prints the path after generating it).
